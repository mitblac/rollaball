﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    //Score and Movement

	public float speed = 0;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;
	public GameObject loseTextObject;

	private Rigidbody rb;
	private int count;
	private float movementX;
	private float movementY;

	//Timer

	//referenced tutorial from Nick Hwang https://www.youtube.com/watch?v=bGePRqD-SNE

	//start time value
	[SerializeField] public float startTime; //Q: what is SerializeField?

	//current Time
	float currentTime;

	//whether timer has started
	bool timerStarted = false;

	//variable for TMP text component
	[SerializeField] TMP_Text timerText;

	// Start is called before the first frame update
	void Start()
    {

		currentTime = startTime;
		timerText.text = "Time: " + currentTime.ToString();
		timerStarted = true;

		rb = GetComponent<Rigidbody>();
		count = 0;

		SetCountText();
		winTextObject.SetActive(false);
		loseTextObject.SetActive(false);
	}

    void OnMove(InputValue movementValue)
	{
		Vector2 movementVector = movementValue.Get<Vector2>();

		movementX = movementVector.x;
		movementY = movementVector.y;
	}

    void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
        if(count >= 15)
		{
			winTextObject.SetActive(true);
			speed = 0;
			rb.gameObject.SetActive(false);
		}
	}

    void FixedUpdate()
	{

		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);
	}

    void Update()
	{
		//for bool, you can do this instead of if timerStarted = true
		if (timerStarted)
		{
			//subtracting time
			currentTime -= Time.deltaTime;
			//timer reaches 0
			if (currentTime <= 0)
			{
				Debug.Log("timer reached zero"); //to check to make sure it works
				timerStarted = false;
				loseTextObject.SetActive(true);
				rb.gameObject.SetActive(false);
				speed = 0;
				currentTime = 0;
			}

			timerText.text = "Time: " + currentTime.ToString("f2"); //f2 sets how far past the decimal point is shown
		}
	}

    private void OnTriggerEnter(Collider other)
	{
        if(other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);
			count += 1;

			SetCountText();
		}
        else if(other.gameObject.CompareTag("TimeBoost"))
		{
			other.gameObject.SetActive(false);
			currentTime += 5;
		}
        else if(other.gameObject.CompareTag("SpeedNerf"))
		{
			other.gameObject.SetActive(false);
			speed -= 4;
		}
	}
}
