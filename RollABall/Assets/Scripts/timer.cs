﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class timer : MonoBehaviour
{

	//referenced tutorial from Nick Hwang https://www.youtube.com/watch?v=bGePRqD-SNE

	//start time value
	[SerializeField] public float startTime; //Q: what is SerializeField?

	//current Time
	float currentTime;

	//whether timer has started
	bool timerStarted = false;

    //variable for TMP text component
    [SerializeField] TMP_Text timerText;

	// Start is called before the first frame update
	void Start()
    {
		currentTime = startTime;
		timerText.text = "Time: " + currentTime.ToString();
		timerStarted = true;
    }

    // Update is called once per frame
    void Update()
    {
		//for bool, you can do this instead of if timerStarted = true
		if (timerStarted)
		{
            //subtracting time
            currentTime -= Time.deltaTime;
            //timer reaches 0
            if(currentTime <= 0)
			{
				Debug.Log("timer reached zero"); //to check to make sure it works
                timerStarted = false;
				currentTime = 0;
			}

            timerText.text = "Time: " + currentTime.ToString("f2"); //f2 sets how far past the decimal point is shown
		}
    }
}
